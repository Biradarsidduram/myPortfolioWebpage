$('document').ready(function(){
	$('a').click(function(){
	    $('html, body').animate({
	        scrollTop: $( $(this).attr('href') ).offset().top
	    }, 800);
	    return false; 
	});
	$('[data-toggle="tooltip"]').tooltip();
	
	$('.chart').easyPieChart({
	    // The color of the curcular bar. You can pass either a css valid color string like rgb, rgba hex or string colors. But you can also pass a function that accepts the current percentage as a value to return a dynamically generated color.
	    barColor: '#000',
	    // The color of the track for the bar, false to disable rendering.
	    trackColor: 'white',
	    // The color of the scale lines, false to disable rendering.
	    scaleColor: false,
	    // Defines how the ending of the bar line looks like. Possible values are: butt, round and square.
	    lineCap: 'round',
	    // Width of the bar line in px.
	    lineWidth: 6,
	    // Size of the pie chart in px. It will always be a square.
	    size: 100,
	    // Time in milliseconds for a eased animation of the bar growing, or false to deactivate.
	    animate: 2000,
	    // Callback function that is called at the start of any animation (only if animate is not false).
	    onStart: $.noop,
	    // Callback function that is called at the end of any animation (only if animate is not false).
	    onStop: $.noop
	  });
 	    $('.chart').each( function(){
	        var value = parseInt($( this ).children('span').text());
	         $( this ).data('easyPieChart').update(value);
	    });

});